/*
 * =====================================================================================
 *
 *       Filename:  log_manager.c
 *
 *    Description:  logging manager
 *
 *        Version:  1.0
 *        Created:  Thursday 20 August 2020 04:18:56  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>

#define INFO 	"INFO"
#define ERROR 	"ERROR"
#define WARN 	"WARN"

void print_log(char* log_type,int line_num,const char* file_name,const char* fun_name,char*fmt,...);
#define LOGI(fmt,args...) print_log(INFO,__LINE__,__FILE__,__FUNCTION__,fmt,##args);
#define LOGE(fmt,args...) print_log(ERROR,__LINE__,__FILE__,__FUNCTION__,fmt,##args);
#define LOGW(fmt,args...) print_log(WARN,__LINE__,__FILE__,__FUNCTION__,fmt,##args);

/**
 * @brief 
 * to get current timestamp in string
 * @return 
 */
char* get_time()
{
	int hours, minutes, seconds, day, month, year;
	time_t now;
	time(&now);
	char *ret_time = malloc(100 * sizeof(char));
	if(!ret_time)
	{
		fprintf(stdout,"Memory error in log\n");
		return NULL;
	}
	memset(ret_time,0,100);
	struct tm *local = localtime(&now);
	hours = local->tm_hour;	/* get hours since midnight (0-23)*/
	minutes = local->tm_min;/* get minutes passed after the hour (0-59)*/
	seconds = local->tm_sec;/* get seconds passed after minute (0-59)*/
	day = local->tm_mday;	/* get day of month (1 to 31)*/
	month = local->tm_mon + 1;	/* get month of year (0 to 11)*/
	year = local->tm_year + 1900;	/* get year since 1900*/
	if (hours < 12)	/* before midday*/
	{
		sprintf(ret_time,"%d-%02d-%02d %02d:%02d:%02d", year, day, month, hours, minutes, seconds);
	}
	else	/* after midday */
	{
		sprintf(ret_time,"%d-%02d-%02d %02d:%02d:%02d",year, day, month, hours - 12, minutes, seconds);
	}
	return ret_time;
}

/**
 * @brief printing log in color full
 *
 * @param log_type INFO/ERROR/WARN
 * @param fmt log str
 * @param ... variable number of argument
 */
void print_log(char* log_type,int line_num,const char* file_name,const char* fun_name,char*fmt,...)
{
	va_list ap;
	char *p,*sval;
	int ival;
	double dval;
	va_start(ap,fmt);
	char str[200] = {0};
	char temp[10] = {0};
	for(p = fmt; *p; p++)
	{
		if(*p != '%')
		{
			sprintf(temp,"%c",*p);
			strcat(str,temp);
			memset(temp,0,sizeof(temp));
			continue;
		}
		switch(*++p)
		{
			case 'd':
				ival = va_arg(ap,int);
				sprintf(temp,"%d",ival);
				strcat(str,temp);
				memset(temp,0,sizeof(temp));
				break;
			case 'f':
				dval = va_arg(ap,double);
				sprintf(temp,"%f",dval);
				strcat(str,temp);
				memset(temp,0,sizeof(temp));
				break;
			case 's':
				for(sval = va_arg(ap,char*); *sval; sval++)
				{
					sprintf(temp,"%c",*sval);
					strcat(str,temp);
					memset(temp,0,sizeof(temp));
				}
				break;
			default:
				temp[0] = va_arg(ap,int);
				strcat(str,temp);
				memset(temp,0,sizeof(temp));
				break;
		}
	}
	va_end(ap); 
	if(!strcmp(log_type,"INFO"))
	{
		fprintf(stdout,"\033[0;32m");//set text color to green
	}
	if(!strcmp(log_type,"ERROR"))
	{
		fprintf(stdout,"\033[0;31m");/* set text color to red */
	}
	if(!strcmp(log_type,"WARN"))
	{
		fprintf(stdout,"\033[0;33m");/* set text color to red */
	}
	char *timestamp = get_time();
	if(timestamp == NULL)
	{
		fprintf(stdout,"error in allocate memory\n");
		return;
	}
	fprintf(stdout,"%s %s [%s] [%s@%d]:%s\n",timestamp,log_type,file_name,fun_name,line_num, str);
	fprintf(stdout,"\033[0m");/* reset text color */
	if(timestamp)
	{
		memset(timestamp,0,strlen(timestamp));
		free(timestamp);
		timestamp = NULL;
	}
	return;
}
