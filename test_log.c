/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  test log
 *
 *        Version:  1.0
 *        Created:  Saturday 22 August 2020 04:48:57  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Kashyap Ekbote (Krash), kashyap.ekbote@gmail.com
 *        Company:  no
 *
 * =====================================================================================
 */

#include <stdio.h>
#include "log_manager.h"

int main()
{
	char str[] = "Kashyap";
	int n = 15;
	char c = 'A';
	double f = 3.14;
	LOGI("str %s int %d char %c float %f ",str,n,c,f);
	LOGE("str %s int %d char %c float %f ",str,n,c,f);
	LOGW("str %s int %d char %c float %f ",str,n,c,f);
}
